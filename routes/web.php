<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/employee/add', function () {
    return view('employee-add');
});

Route::get('/company/add', function () {
    return view('company-add');
});

Route::get('/employee/manage', function () {
    return view('employee-manage');
});

// Route::get('/employee/manage', function () {
//     return view('employee-manage');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
