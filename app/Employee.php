<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'firstname',
        'lastname',
        'company',
        'email',
        'phone'
        
    ];

     public function company()
    {
        return $this->hasMany('App\Employee', 'company');
    }

    public static function getValidationRule () {
        return [
            'firstname' => 'required|',
            'lastname' => 'required',
            'company' => 'required',
            'email' => 'required|email',
            'phone' => 'numeric'
        ]
}
