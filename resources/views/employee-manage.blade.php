 @extends('layouts.admin')

@section('content')
<div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Manage Employees</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>
                        First Name
                      </th>
                      <th>
                        Last Name
                      </th>
                      <th>
                        Company
                      </th>
                      <th class="text-right">
                        Email
                      </th>
                      <th class="text-right">
                        Phone
                      </th>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          Dakota
                        </td>
                        <td>
                        	Rice
                        </td>
                        <td>
                          Niger LTD
                        </td>
                        <td>
                          hello@com.com
                        </td>
                        <td class="text-right">
                          080200001001
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <nav class="footer-nav">
            </nav>
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script>, Developed by Tosin Adeniyi
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  @endsection